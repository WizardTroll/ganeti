	## Auto Ubuntu 19.05 AUTOINSTALL19.iso script ##
		### multibootusb installing multiple distros without user intervention
		### mkusb wipewholedevice
		### 	##Testing if UEFI or BIOS
test -d /sys/firmware/efi && echo efi || echo bios
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 

	# Created the /mnt/iso19 directory for editing.

sudo mkdir -p /mnt/iso19

	# Mounted Ubuntu /mnt/iso19 to copy the contents to the /mnt/iso19 directory and later edit the pertinent files for our auto install19

sudo mount -o loop ubuntu-19.04-desktop-amd64.iso /mnt/iso19

	# Created directory /opt/ubuntuiso18

sudo mkdir -p /opt/ubuntuiso19

	# Copied the iso19 files to the /opt/ubuntuiso19 directory for editing.

sudo cp -rT /mnt/iso19 /opt/ubuntuiso19

	# Nano /opt/ubuntuiso19/isolinux/isolinux.cfg

sudo nano /opt/ubuntuiso19/isolinux/isolinux.cfg

 	# Edited (ctrl+shift+V) the /opt/ubuntuiso19/isolinux/isolinux.cfg file and replaced everything inside with the following code ## (Removed extensions for 'kernel /casper/vmlinuz.efi' and 'initrd=/casper/initrd.lz'
		### Added this URL in this menu
		### https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg
		### url=https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg


default live-install
label live-install
  menu label ^Install Ubuntu
  kernel /casper/vmlinuz
  append  file=/cdrom/ks19.preseed auto=true priority=critical debian-installer/locale=en_US keyboard-configuration/layoutcode=us ubiquity/reboot=true languagechooser/language-name=English countrychooser/shortlist=US localechooser/supported-locales=en_US.UTF-8 boot=casper automatic-ubiquity initrd=/casper/initrd quiet splash noprompt noshell ---

	# Copied custom pressed file from the Desktop/opt/ubuntuiso19/ks.19.preseed to the root of the ubuntuiso19 inside the multi boot image.
		###----- Hashed out auto=install on the first line of the preseed file
		### ---- Using original preseed without modifications (23 July)
		### Fixed period in the new script from https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg (24 July)
		### Custom Partitioning settings derived from the original https://gitlab.com/denny77/ganeti/raw/test_other_preseed/preseed.cfg (July 24)
		### Trying to override the partitioning confirmation messages (July 24)
		### Added Aps Sectioning to full-upgrade (July 24)
		### Added in the Aps Sectioning to install gparted (July 24)

sudo cp /home/volunteer/Desktop/ks19.preseed /opt/ubuntuiso19
sudo cp ks19.preseed /opt/ubuntuiso19
	# Created the new iso: -- autoinstall19.iso -- from the the /opt/ubuntuiso19/ directory.
		### Edited ISO label to indicate ATTENDLESS_UBUNTU19 (July 24)

sudo mkisofs -D -r -V ATTENDLESS_UBUNTU19 -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o /opt/autoinstall19.iso /opt/ubuntuiso19

	# Iso Hybrid command (to boot)
sudo isohybrid /opt/autoinstall19.iso

	# Single boot writing ISO image to target USB disk (will destroy data on USB disk):

sudo multibootusb -c -r -i /opt/autoinstall19.iso -t /dev/sdb

	# multibootusb installing multiple distros without user intervention:
#sudo multibootusb -c -y -i /opt/autoinstall18.iso,/opt/autoinstall19.iso -t /dev/sdb1
	

	## CLEAN UP ##
	# Wipe the USB device (may take long time)
#sudo -H /usr/sbin/mkusb-11 wipe
	# ---- Wipe the first megabyte (MibiByte)
#sudo -H /usr/sbin/mkusb-11 wipe-whole-device

	# show only USB devices
#sudo -H /usr/sbin/mkusb-11 wipe-1
	
	# Removing the autoinstall19 and the ubuntuiso19 directories recursively

#sudo rm -rf /opt/autoinstall19.iso
#sudo rm -rf /opt/ubuntuiso19/ks19.preseed

#sudo rm -rf /opt/ubuntuiso19
#sudo rm -rf /mnt/iso19







	# Make USB (DONE)
	# https://help.ubuntu.com/community/mkusb

# sudo add-apt-repository universe  # only for standard Ubuntu
# sudo add-apt-repository ppa:mkusb/ppa  # and press Enter
# sudo apt-get update
# sudo apt-get install mkusb mkusb-nox usb-pack-efi
























